from django.shortcuts import render
from django.utils import timezone

from .models import News

# Create your views here.

def news_list(request):
    posts = News.objects.all()

    return render(request, 'news/news_list.html', {'posts': posts})
