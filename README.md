# Instructions

- Create virtual env inside project directory [http://docs.python-guide.org/en/latest/dev/virtualenvs/]

- Install requirements `$ pip install -r requirements.txt`. This command will install the requirements from the provided requirements.txt file.

# Additional

- Make migrations for models with changes `$ python3 manage.py makemigrations`

- Migrate `$ python3 manage.py migrate`

- Create superuser `$ python3 manage.py createsuperuser`

- Run server `$ python3 manage.py runserver`